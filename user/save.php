<?php

require_once '../config.php';
require_once BASE . 'connection.php';
require_once BASE . 'message.php';

$id = $name = $email = $password = '';
extract($_POST);

if ($id) {
	$id = (int)$id;
	$query = "UPDATE users SET name='$name', email='$email', password='$password' WHERE id=$id";
} else {
	$query = "INSERT INTO users (name, email, password) VALUES ('$name', '$email', '$password')";
}

$result = mysqli_query($con, $query);

if ($result) {
	addMessage('success', 'Cadastrado com sucesso.');
	header("location: index.php");
} else {
	$error = mysqli_error($con);
	$message = strpos($error, 'Duplicate entry') >= 0 ? 'E-mail já cadastrado' : 'Erro inesperado';
	addMessage('danger', $message);
	header("location: form.php?status=error&message=$message");
}

?>
