<?php

require_once '../config.php';
require_once BASE . 'connection.php';
require_once BASE . 'message.php';
require_once BASE . 'permission.php';

$query = "SELECT * FROM users";
$result = mysqli_query($con, $query);

?><!DOCTYPE html>
<html>
	<?php include_once BASE . 'head.php'; ?>
	<body>
	<?php include_once BASE . 'nav.php'; ?>
		<div class="container">
			<?php include_once BASE . 'message_html.php'; ?>
			<h1>Usuários</h1>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Nome</th>
						<th>Email</th>
						<th colspan="2">Ações</th>
					</tr>
				</thead>
				<tbody>
					<?php while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) { ?>
					<tr>
						<td><?php echo $row['name'] ?></td>
						<td><?php echo $row['email'] ?></td>
						<td>
							<a href="form.php?id=<?php echo $row['id'] ?>">
								Alterar
							</a>
						</td>
						<td>
							<a onclick="return confirmDelete()" href="delete.php?id=<?php echo $row['id'] ?>">
								Excluir
							</a>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
			<a href="form.php" class="btn btn-primary">Inserir</a>
		</div>
	</body>
</html>
