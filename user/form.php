<?php

require_once '../config.php';
require_once BASE . 'connection.php';
require_once BASE . 'message.php';
require_once BASE . 'permission.php';

/*campos do banco*/
$id = $name = $email = '';

if (isset($_GET['id'])) {
	$id = (int)$_GET['id'];
	$query = "SELECT * FROM users WHERE id=$id";
	$result = mysqli_query($con, $query);
	$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
	extract($row);
	$h1 = 'Alterando';
} else {
	$h1 = 'Inserindo';
}

?><!DOCTYPE html>
<html>
	<?php include_once BASE . 'head.php'; ?>
	<body>
		<?php include_once BASE . 'nav.php'; ?>
		<div class="container">
			<?php include_once BASE . 'message_html.php'; ?>
			<h1><?php echo $h1 ?> usuário</h1>
			<form action="save.php" method="post" class="form-signin">
				<div class="form-group">
					<label>Nome:</label>
					<input type="text" name="name" value="<?php echo $name ?>" class="form-control">
				</div>
				<div class="form-group">
					<label>E-mail:</label>
					<input type="email" name="email" value="<?php echo $email ?>" class="form-control">
				</div>
				<div class="form-group">
					<label>Senha:</label>
					<input type="password" name="password" class="form-control">
				</div>
				<div class="form-group">
					<input type="hidden" name="id" value="<?php echo $id ?>">
					<input type="submit" value="Salvar" class="btn btn-lg btn-primary btn-block">
				</div>
			</form>
		</div>
	</body>
</html>