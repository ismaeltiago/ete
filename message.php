<?php

function addMessage($status, $message) {
	if (!isset($_SESSION['messages'])) {
		$_SESSION['messages'] = [];
	}

	if (!isset($_SESSION['messages'][$status])) {
		$_SESSION['messages'][$status] = [];
	}

	$_SESSION['messages'][$status][] = $message;
}

function getMessages() {
	if (!isset($_SESSION['messages'])) {
		return [];
	}

	$messages = $_SESSION['messages'];
	unset($_SESSION['messages']);

	return $messages;
}

?>
