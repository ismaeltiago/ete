<?php

function cssFiles() {
	global $append_css;

	if (!$append_css) {
		$append_css = [];
	}

	$default_files = [
		'http://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css',
		URL . 'css/bootstrap-theme.min.css',
		URL . 'css/style.css'
	];

	return array_merge($default_files, $append_css);
}

function javascriptFiles() {
	global $append_javascript;

	if (!$append_javascript) {
		$append_javascript = [];
	}

	$default_files = [
		'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js',
		'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',
		URL . 'js/script.js'
	];

	return array_merge($default_files, $append_javascript);
}

?>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>
	<?php foreach (cssFiles() as $cssFile) : ?>
	<link rel="stylesheet" type="text/css" href="<?php echo $cssFile ?>">
	<?php endforeach;
	foreach (javascriptFiles() as $javascriptFile) : ?>
	<script type="text/javascript" src="<?php echo $javascriptFile ?>"></script>
	<?php endforeach; ?>
</head>
