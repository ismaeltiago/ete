<?php

function activeLink($link) {
	$request_uri = explode('/', $_SERVER['REQUEST_URI']);
	$controller_index = count($request_uri) - 2;

	if (isset($request_uri[$controller_index])) {
		$controller_name = '/' . $request_uri[$controller_index] . '/';
	} else {
		$controller_name = null;
	}

	return (strpos($controller_name, "/$link/") !== false);
}

function activeLinkClass($link, $css_class = 'active') {
	return activeLink($link) ? $css_class : null;
}

?>
<nav class="navbar navbar-default">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand">Blog</a>
		</div>
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li class="<?php echo activeLinkClass('user') ?>"><a href="<?php echo URL . 'user' ?>">Usuários</a></li>
				<li class="<?php echo activeLinkClass('post') ?>"><a href="<?php echo URL . 'post' ?>">Posts</a></li>
				<li><a href="<?php echo URL . 'painel/index.php' ?>">Sair</a></li>
			</ul>
		</div><!--/.nav-collapse -->
	</div>
</nav>
