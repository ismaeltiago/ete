<?php

define("SERVER", '127.0.0.1');
define("USER", 'root');
define("PASSWORD", '');
define("DATABASE", 'ete');

$con = mysqli_connect(SERVER, USER, PASSWORD, DATABASE);

if (!$con) {
	echo 'DATABASE ERROR';
	exit;
}

mysqli_query($con, 'SET NAMES UTF8;');

?>
