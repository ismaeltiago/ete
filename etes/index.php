<?php

require_once '../config.php';
require_once BASE . 'connection.php';
require_once BASE . 'message.php';
require_once BASE . 'permission.php';

$query = "SELECT * FROM etes";
$result = mysqli_query($con, $query);

?><!DOCTYPE html>
<html>
	<?php include_once BASE . 'head.php'; ?>
	<body>
	<?php include_once BASE . 'nav.php'; ?>
		<div class="container">
			<?php include_once BASE . 'message_html.php'; ?>
			<h1>ETE´s</h1>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Nome</th>
						<th>Vazão Projeto</th>
						<th>Endereço</th>
						<th colspan="2">Ações</th>
					</tr>
				</thead>
				<tbody>
					<?php while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) { ?>
					<tr>
						<td><?php echo $row['name'] ?></td>
						<td><?php echo $row['flow_rate'] ?></td>
						<td><?php echo $row['address'] ?></td>
						<td>
							<a href="form.php?id=<?php echo $row['id'] ?>">
								Alterar
							</a>
						</td>
						<td>
							<a onclick="return confirmDelete()" href="delete.php?id=<?php echo $row['id'] ?>">
								Excluir
							</a>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
			<a href="form.php" class="btn btn-primary">Inserir</a>
		</div>
	</body>
</html>