<?php
require_once '../config.php';
?>
<html lang = "pt-br">

	<head>
		<meta charset="utf-8">
		<title>Controle ETE´s</title>

		<link rel="stylesheet" type="text/css" href="../css/style.css">
	
	</head>
	
	<body style="background-color: white"> <!--#dfdfdf">-->
	<!-- cod tela 'style="background-color: blue"> -->
			
	<header> 
		<!--<h1>Coordenadoria de Estações de Tratamento de Esgoto</h1>-->


	</header>
			<!--<div id="logo" style="text-align: center; width: 60%; float: left;"> CONTROLE OPERACIONAL ETE´s - SC</div> -->
		<!--Chama arquivo logim   HOME.html-->
		 
		<img src="../imagem/Logo_Samae.gif" width="180px" height="70px" align="right, float: center" />
			
		<p><div class="menu-container">
			<ul class="menu clearfix"> <!-- MENU principal -->
				<li><a href="login/logout.php">HOME</a></li>

				<li>
				<a href="ETE´s.html">ETE´S</a>
				<ul class="sub-menu clearfix"> <!--Sub-menu-->
					<li>
					<a href="#">ETE ÁGUA VERDE</a>
					<ul class="Sub-menu"> <!--Sub-niveis do submenu-->
						<li><a href="#">DADOS</a></li>
						<li><a href="#">SERVIDORES</a></li>
						<li><a href="#">FOTOS</a></li>
					</ul>
					</li>
						<li>
							<a href="#">ETE FIGUEIRA</a>
							<ul class="Sub-menu"> <!--Sub-niveis do submenu-->
							<li><a href="#">DADOS</a></li>
							<li><a href="#">SERVIDORES</a></li>
							<li><a href="#">FOTOS</a></li>
						</ul>
						</li>
							<li>
								<a href="#">ETE NEREU</a>
								<ul class="Sub-menu"> <!--Sub-niveis do submenu-->
								<li><a href="#">DADOS</a></li>
								<li><a href="#">SERVIDORES</a></li>
								<li><a href="#">FOTOS</a></li>
								</ul>
							</li>
								<li>
									<a href="#">ETE SÃO LUIS</a>
									<ul class="Sub-menu"> <!--Sub-niveis do submenu-->
									<li><a href="#">DADOS</a></li>
									<li><a href="#">SERVIDORES</a></li>
									<li><a href="#">FOTOS</a></li>
								</ul>
							</li>	
						</li>
					</ul>
				</li>
				
				<li>
				<a href="cadastrar.php">CONTROLE OPERACIONAL</a>
				<ul class="sub-menu clearfix"> <!--Sub-menu-->
					<li>
					<a href="#">TRATAMENTO ESGOTO</a>
						<ul class="Sub-menu"> <!--Sub-niveis do submenu-->
						<li>
						<a href="#">ETE ÁGUA VERDE</a>
							<ul class="Sub-menu"> <!--Sub-niveis do submenu-->
							<li><a href="#">Análises Operacionais</a></li>
							<li><a href="<?php echo URL . 'macromedicoes/form.php' ?>">Macromedições</a></li>
							<li><a href="#">Descargas</a></li>
							<li><a href="#">Controle Biofiltros de gases</a></li>
							<li><a href="#">Controle Diversos</a></li>
						</ul>
						</li>
							<li>
							<a href="#">ETE FIGUEIRA</a>
								<ul class="Sub-menu"> <!--Sub-niveis do submenu-->
								<li><a href="#">Análises Operacionais</a></li>
								<li><a href="#">Macromedições</a></li>
								<li><a href="#">Descargas</a></li>
								<li><a href="#">Controle Biofiltros de gases</a></li>
								<li><a href="#">Controle Diversos</a></li>
							</ul>
							</li>
								<li>
								<a href="#">ETE NEREU</a>
									<ul class="Sub-menu"> <!--Sub-niveis do submenu-->
									<li><a href="#">Análises Operacionais</a></li>
									<li><a href="#">Macromedições</a></li>
									<li><a href="#">Descargas</a></li>
									<li><a href="#">Controle Biofiltros de gases</a></li>
									<li><a href="#">Controle Diversos</a></li>
								</ul>
								</li>
									<li>
									<a href="#">ETE SÃO LUIS</a>
										<ul class="Sub-menu"> <!--Sub-niveis do submenu-->
										<li><a href="#">Análises Operacionais</a></li>
										<li><a href="#">Macromedições</a></li>
										<li><a href="#">Descargas</a></li>
										<li><a href="#">Controle Biofiltros de gases</a></li>
										<li><a href="#">Controle Diversos</a></li>
										</ul>
									</li>						
								</ul>
							</li>
						<li>
						<a href="#">TRATAMENTO LODO</a>
						<ul class="Sub-menu"> <!--Sub-niveis do submenu-->
						<li>
						<a href="#">ETE ÁGUA VERDE</a>
							<ul class="Sub-menu"> <!--Sub-niveis do submenu-->
							<li><a href="#">Desaguamento de Lodo</a></li>
							<li><a href="#">Adensamento de Lodo</a></li>	
						</ul>
						</li>
							<li>
							<a href="#">ETE FIGUEIRA</a>
								<ul class="Sub-menu"> <!--Sub-niveis do submenu-->
								<li><a href="#">Desaguamento de Lodo</a></li>
								<li><a href="#">Adensamento de Lodo</a></li>
								</ul>
							</li>
								<li>
								<a href="#">ETE NEREU</a>
									<ul class="Sub-menu"> <!--Sub-niveis do submenu-->
									<li><a href="#">Desaguamento de Lodo</a></li>
									<li><a href="#">Adensamento de Lodo</a></li>
									</ul>
								</li>
									<li>
									<a href="#">ETE SÃO LUIS</a>
										<ul class="Sub-menu"> <!--Sub-niveis do submenu-->
										<li><a href="#">Desaguamento de Lodo</a></li>
										<li><a href="#">Adensamento de Lodo</a></li>
										</ul>
									</li>						
								</ul>
							</li>
						</ul>	
					</li>
				<li><a href="CRONOGRAMA ATIVIDADES.html">CRONOGRAMA ATIVIDADES</a></li>
				<li><a href="SERVIÇOS.html">SERVIÇOS</a></li>
				<li><a href="PEDIDOS.html">PEDIDOS</a></li>
				<li><a href="CONTRATOS.html">CONTRATOS</a></li>
				<li><a href="PLANO AÇÕES.html">PLANO AÇÕES</a></li>
				<li><a href="GERENCIAMENTO.html">GERENCIAMENTO</a></li>
			</ul>
		</div>
		

		
		<!-- COMENTANDO FORM - SELECT DAS ETES-->
		<!--<form id="formeETEAV" method="post" action="cadastrar.php">
		<br><br>
		<P>SAMAE JARAGUÁ DO SUL/SC</P><hr>
        <p><label> ESTAÇÃO:</label>
            <select name ="t_estacao" id="c_estacao">
        	<option value="ETE ÁGUA VERDE">ETE ÁGUA VERDE</option>
        	<option value="ETE FIGUEIRA">ETE FIGUEIRA</option>
			<option value="ETE NEREU">ETE NEREU</option>	
			<option value="ETE SÃO LUIS">ETE SÃO LUIS</option>
        </select> 
			<br><br>
		<!-- <p><a href="cadastrar.php"></a> id="cestacao"<p> -->
		<!--<p><a href="cadastrar.php">Cadastrar</a><p> -->
		<!--<p><a href="relatorio.php">Relatório</a><p> --> 

		<!--<input id="submeter" type="submit" value="ABRIR CONTROLE">-->	
		<!--</form>-->

	</body>

</html> 
	