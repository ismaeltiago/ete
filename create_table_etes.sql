CREATE TABLE IF NOT EXISTS `etes` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `flow_rate` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `etes`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `etes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
