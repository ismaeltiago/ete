CREATE DATABASE ete CHARACTER SET UTF8 COLLATE utf8_general_ci;
USE ete;

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `posts`
  ADD CONSTRAINT `posts_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

INSERT INTO `users` (`name`, `email`, `password`) VALUES ('Admin', 'admin@admin.com.br', 'admin');

INSERT INTO `posts` (`id`, `user_id`, `title`, `description`) VALUES (1, 1, 'PHP do 5.6 para o 7.0', 'Como assim a 5.6 precede a 7.0 e não a 6.0?\r\nAh, uma longa história ... O PHP, sim, pulou da versão 5.6 para a 7.0.\r\nNão foi na surdina da noite como fazem alguns dos nossos políticos com as suas pautas emergenciais. A decisão de pular a versão 6.0 foi discutida por várias meses e votada de forma honrosa.\r\n\r\nHá cerca de 10 anos o PHP teve um esboço iniciado do que seria a futura versão 6.0. Mas o planejamento não foi muito legal e o core ficou bastante "prejudicado" com uma alteração importante que dava à linguagem suporte nativo a Unicode. Paralelamente a isso, versões menores continuaram a ser lançadas (como a 5.3) e não conseguiram o êxito de manter os branchs sincronizados. Com isso, naturalmente, o projeto da versão 6.0 entrou em colapso, só que, antes disso, diversos autores começaram a publicar livros sobre o "PHP 6.0" (que nem estavelmente ainda houvera sido lançado). Então, pelo bem de todos (inclusive dos futuros desenvolvedores da linguagem), decidiram pular a 6.0 e fazer tudo certinho na 7.0.\r\n\r\nLeia a versão completa em https://www.treinaweb.com.br/blog/php-7-e-novidades-do-php-7-1/');
INSERT INTO `posts` (`id`, `user_id`, `title`, `description`) VALUES (2, 1, 'Composer: a luz no fim do túnel', 'Composer é uma ferramenta para gerenciamento de dependências em PHP. Ele permite que você declare as bibliotecas dependentes que seu projeto precisa e as instala para você.\r\n\r\nPara quem não está acostumado ou nunca ouviu falar em Gerenciamento de Dependências, pode não ter ficado claro (ou difícil de entender por ser bom demais para ser verdade), mas, na prática, o que acontece é que, usando Composer, você simplesmente especifica quais pacotes (códigos reutilizáveis) seu projeto precisa – podendo estes pacotes também ter dependências – e ele vai, automaticamente, baixar isso e incluir nos locais apropriados de seu projeto!\r\n\r\nLeia a versão completa em http://desenvolvimentoparaweb.com/php/composer-a-evolucao-php/');
INSERT INTO `posts` (`id`, `user_id`, `title`, `description`) VALUES (3, 1, 'MySQL e UTF-8', 'CHARSET e COLLATE são coisas distintas, no MySQL, cada CHARSET possui COLLATEs, cada um com sua particularidade. O intuito deste Wiki não é explicar as características de cada um deles, pois pode ser visto da documentação do MySQL, mas daremos um pequeno descritivo entre latin1_general_ci, latin1_general_cs e latin1_swedish_ci.\r\n\r\n- latin1_general_ci: Não há distinção entre letras maiúsculas e minúsculas. Buscando por "teste", registros como "Teste" ou "TESTE" serão retornados.\r\n- latin1_general_cs: Distingue letras maiúsculas e minúsculas. Buscando por "teste" somente retornará "teste". Opções como "Teste" e "TESTE" não serão retornadas.\r\n- latin1_swedish_ci: Não distingue letras minúsculas e maiúsculas e nem caracteres acentuados e com cedilha, ou seja, o registro que contém a palavra "Intuição" será retornado quando houver uma procura pela palavra "intúicao".\r\n\r\nLeia a versão completa em https://wiki.locaweb.com.br/pt-br/Resolvendo_problemas_de_caracteres_acentuados_no_MySQL');
