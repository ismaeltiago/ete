<?php

require_once '../config.php';
require_once BASE . 'connection.php';
require_once BASE . 'message.php';
require_once BASE . 'permission.php';

$query = "SELECT posts.id, posts.title, users.name FROM posts JOIN users ON (users.id=posts.user_id)";
$result = mysqli_query($con, $query);

?><!DOCTYPE html>
<html>
	<?php include_once BASE . 'head.php'; ?>
	<body>
	<?php include_once BASE . 'nav.php'; ?>
		<div class="container">
			<?php include_once BASE . 'message_html.php'; ?>
			<h1>Posts</h1>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Título</th>
						<th>Autor</th>
						<th colspan="3">Ações</th>
					</tr>
				</thead>
				<tbody>
					<?php while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) { ?>
					<tr>
						<td><?php echo $row['title'] ?></td>
						<td><?php echo $row['name'] ?></td>
						<td>
							<a href="view.php?id=<?php echo $row['id'] ?>">
								Ver
							</a>
						</td>
						<td>
							<a href="form.php?id=<?php echo $row['id'] ?>">
								Alterar
							</a>
						</td>
						<td>
							<a onclick="return confirmDelete()" href="delete.php?id=<?php echo $row['id'] ?>">
								Excluir
							</a>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
			<a href="form.php" class="btn btn-primary">Inserir</a>
		</div>
	</body>
</html>