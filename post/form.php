<?php

require_once '../config.php';
require_once BASE . 'connection.php';
require_once BASE . 'message.php';
require_once BASE . 'permission.php';

$id = $title = $description = $user_id = '';

if (isset($_GET['id'])) {
	$id = (int)$_GET['id'];
	$query = "SELECT * FROM posts WHERE id=$id";
	$result = mysqli_query($con, $query);
	$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
	extract($row);
	$h1 = 'Alterando';
} else {
	$h1 = 'Inserindo';
}

$query = 'SELECT id, name FROM users ORDER BY name';
$result = mysqli_query($con, $query);

?><!DOCTYPE html>
<html>
	<?php include_once BASE . 'head.php'; ?>
	<body>
		<?php include_once BASE . 'nav.php'; ?>
		<div class="container">
			<?php include_once BASE . 'message_html.php'; ?>
			<h1><?php echo $h1 ?> post</h1>
			<form method="post" action="save.php" class="form-signin">
				<div class="form-group">
					<label>Autor:</label>
					<select name="user_id" class="form-control">
						<?php while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
							$selected = $user_id == $row['id'] ? 'selected' : '';
							?>
						<option <?php echo $selected ?> value="<?php echo $row['id'] ?>"><?php echo $row['name'] ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="form-group">
					<label>Título:</label>
					<input type="text" name="title" value="<?php echo $title ?>" class="form-control">
				</div>
				<div class="form-group">
					<label>Descrição:</label> 
					<textarea name="description" placeholder="Descrição" class="form-control" rows="10" ><?php echo $description ?></textarea>
				</div>
				<div class="form-group">
					<input type="hidden" name="id" value="<?php echo $id ?>">
					<input type="submit" value="Salvar" class="btn btn-lg btn-primary btn-block">
				</div>
			</form>
		</div>
	</body>
</html>