<?php

require_once '../config.php';
require_once BASE . 'connection.php';
require_once BASE . 'message.php';
require_once BASE . 'permission.php';

if (isset($_GET['id'])) {
	$id = (int)$_GET['id'];
	$query = "SELECT posts.id, posts.title, posts.description, users.name FROM posts INNER JOIN users ON (users.id = posts.user_id) WHERE posts.id=$id";
	$result = mysqli_query($con, $query);
	$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
} else {
	addMessage('warning', 'Post não identificado.');
	header('location: index.php');
}

?><!DOCTYPE html>
<html>
	<?php include_once BASE . 'head.php'; ?>
	<body>
		<?php include_once BASE . 'nav.php'; ?>
		<div class="container">
			<?php include_once BASE . 'message_html.php'; ?>
			<h1>Post</h1>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title"><?php echo $row['title'] ?></h3>
				</div>
				<div class="panel-body">
					<p>Autor: <?php echo $row['name'] ?></p>
					<div>
						<?php echo nl2br($row['description']) ?>
					</div>
				</div>
			</div>
			<a href="form.php?id=<?php echo $row['id'] ?>" class="btn btn-primary">Alterar</a>
			<a href="index.php" class="btn btn-default">Posts</a>
		</div>
	</body>
</html>