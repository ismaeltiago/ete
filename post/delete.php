<?php

require_once '../config.php';
require_once BASE . 'connection.php';
require_once BASE . 'message.php';
require_once BASE . 'permission.php';

$id = (int)$_GET['id'];
$query = "DELETE FROM posts WHERE id=$id";
$result = mysqli_query($con, $query);

if ($result) {
	$status = 'success';
	$message = 'Post excluído com sucesso.';
} else {
	$status = 'danger';
	$message = 'Erro ao excluir post.';
}

addMessage($status, $message);
header("location: index.php");

?>
