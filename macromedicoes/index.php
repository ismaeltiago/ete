<?php 
require_once '../config.php';
require_once BASE . 'connection.php';
require_once BASE . 'message.php';
require_once BASE . 'permission.php';

$query = "SELECT macromedicoes.id, macromedicoes.ete, users.name FROM macromedicoes JOIN users ON (users.id=macromedicoes.user_id)";
$result = mysqli_query($con, $query);

?><!DOCTYPE html>
<html>
	<?php include_once BASE . 'head.php'; ?>
	<body>
	<?php include_once BASE . 'nav.php'; ?>
		<div class="container">
			<?php include_once BASE . 'message_html.php'; ?>
			<h1>MACROMEDIÇÕES</h1>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Data</th>
						<th>Hora</th>
						<th>ETE</th>
						<th>Nome</th>
						<th>Leitura</th>
						<th>FaTor Leitura</th>
						<th colspan="3">Ações</th>
					</tr>
				</thead>