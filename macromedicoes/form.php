<?php 

require_once '../config.php';
require_once BASE . 'connection.php';
require_once BASE . 'message.php';
require_once BASE . 'permission.php';
/* campos da tabela do arquivo*/
$id = $data = $hora = $ete = $name = $leitura = $fatorleitura = $observacao = '';

/* 2 parte*/
if (isset($_GET['id'])) {
	$id = (int)$_GET['id'];
	$query = "SELECT * FROM macromedicoes WHERE id=$id";
	$result = mysqli_query($con, $query);
	$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
	extract($row);
	$h1 = 'Alterando';
} else {
	$h1 = 'Inserindo';
}

/*3parte*/
$query = 'SELECT id, name FROM users ORDER BY name';
$result = mysqli_query($con, $query);

/*4parte*/
?><!DOCTYPE html>
<html>
	<?php include_once BASE . 'head.php'; ?>
	<!--Boostrap datapicker-->
		<link rel="stylesheet"  href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		
		<link href="../css/bootstrap-datepicker.css" rel="stylesheet"/>
		<script src="../js/bootstrap-datepicker.min.js"></script> 
		<script src="../js/bootstrap-datepicker.pt-BR.min.js" charset="UTF-8"></script>
	<body>
		<?php include_once BASE . 'nav.php'; ?>
		<div class="container">
			<?php include_once BASE . 'message_html.php'; ?>
			<h1><?php echo $h1 ?> Macromedições</h1>
			<form method="post" action="save.php" class="form-signin">
				<div class="form-group">
					<!--label>Técnico:</label>-->
					<select name="user_id" class="form-control">
						<?php while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
							$selected = $user_id == $row['id'] ? 'selected' : '';
							?>
						<option <?php echo $selected ?> value="<?php echo $row['id'] ?>"><?php echo $row['name'] ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="form-group">
					<!--label>Ete:</label>-->
					<select name="ete" value="<?php echo $ete ?>" class="form-control" >
						<option value="1">ETE ÁGUA VERDE</option>
						<option value="2">ETE FIGUEIRA</option>
						<option value="1">ETE NEREU</option>
						<option value="1">ETE SÃO LUIS</option>
					</select>
					<!--<input type="text" name="estaçao" value="<?php echo $ete ?>" class="form-control"> -->
				</div>
				<div class="form-group">
					<!--<label>Data:</label>-->
					<label class="col-sm-2 control-label"></label>
					<div class="col-sm-10">
						<div class="input-group date">
							<input type="text" placeholder="Data" class="form-control" id="exemplo" value="<?php echo $data ?>" class="form-control">
							<div class="input-group-addon">
							<span class="glyphicon glyphicon-th"></span>
							</div>
						</div>
					</div>
				</div><br></br><br>
				<div class="form-group">
					<!--label>Hora:</label>-->
					<input type="Time" placeholder="Hora" name="hora" value="<?php echo $hora ?>" class="form-control">
				</div>
				<div class="form-group">
					<!--label>Leitura:</label>-->
					<input type="number" placeholder="Leitura" name="leitura" min="0" max="999999" value="<?php echo $leitura ?>" class="form-control">
				</div>
				<div class="form-group">
					<!--label>fator:</label>-->
					<input type="number" placeholder="fatorleitura" name="fator" value="<?php echo $fatorleitura ?>" class="form-control">
				</div>
				<div class="form-group">
					<!--label>Observação:</label>-->
					<textarea name="observacao" placeholder="Observação" class="form-control" rows="2"><?php echo $observacao ?></textarea>
				</div>
				<div class="form-group">
					<input type="hidden" name="id" value="<?php echo $id ?>">
					<input type="submit" value="Salvar" class="btn btn-lg btn-primary btn-block">
				</div>
			</form>
		</div>
		<script type="text/javascript">
			$('#exemplo').datepicker({	
				format: "dd/mm/yyyy",	
				language: "pt-BR",
				startDate: '+0d',
			});
		</script>
	</body>
</html>
