<?php

require_once '../config.php';
require_once BASE . 'message.php';

if (isset($_SESSION['user_email'])) {
	header('location: ../user'); // location: ../teste
	exit;
}

$append_css = [
	URL . 'css/signin.css'
];

$cookie_email = isset($_COOKIE['email']) ? $_COOKIE['email'] : '';

?><!DOCTYPE html>
<html>
	<?php include_once BASE . 'head.php'; ?>
	<body>
		<div class="container form-signin">
			<?php include_once BASE . 'message_html.php'; ?>
    		<h1>Login</h1>
			<form method="post" action="login.php">
				<fieldset>
					<input type="email" name="email" value="<?php echo $cookie_email ?>" placeholder="E-mail" class="form-control">
				</fieldset>
				<fieldset>
					<input type="password" name="password" placeholder="Senha" class="form-control">
				</fieldset>
				<fieldset>
					<input type="submit" value="Entrar" class="btn btn-lg btn-primary btn-block">
				</fieldset>
			</form>

			<div class="panel panel-default page-header">
				<div class="panel-heading">
					<h3 class="panel-title">Primeiro acesso</h3>
				</div>
				<div class="panel-body">
					E-mail: <b>admin@admin.com.br</b><br>
					Senha <b>admin</b>
				</div>
			</div>

		</div>
	</body>
</html>
